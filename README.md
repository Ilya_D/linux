
# *Установка обновлений -* 
	sudo apt list --upgradable -a && sudo apt update && sudo apt full-upgrade -y

# *Очистка системы от мусора -* 
	sudo apt-get autoclean && sudo apt-get clean && sudo apt-get autoremove && sudo apt-get -f install 

# *Установка необходимых программ для робототехники -* 
	cd ~ ; sudo apt install -y git && git clone https://github.com/rurewa/all_install.git && cd all_install ; ./install.sh
